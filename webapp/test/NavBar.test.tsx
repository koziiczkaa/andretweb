import * as React from 'react';
import {shallow, ShallowWrapper} from 'enzyme';
import {NavBar} from '../src/components/NavBar';

describe('NavBar', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<NavBar id={'navbar'} language={[
            {language: 'DE', img: '../webapp/resources/images/de-icon.png'},
            {language: 'EN', img: '../webapp/resources/images/gb-icon.png'},
            {language: 'PL', img: '../webapp/resources/images/pl-icon.png'}
        ]}/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
