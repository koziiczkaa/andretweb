import {shallow, ShallowWrapper} from "enzyme";
import {ProjectsPage} from "../src/pages/ProjectsPage";
import React, {EffectCallback} from "react";

describe('ProjectsPage', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<ProjectsPage project={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>)

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('useEffect', ()=>{
        //given
        window.scrollTo = jest.fn();
        jest.spyOn(React, 'useEffect').mockImplementation((f: EffectCallback): void | (() => void | undefined) => f());

        //when
        const wrapper: ShallowWrapper = shallow(<ProjectsPage project={[{
            title: 'test',
            desc: 'test',
            img: 'testimg',
            id: 1,
            projectReference: [{
                name: 'GitLab',
                link: 'https://gitlab.com',
                icon: <></>
            }]
        }]}/>)

        //then
        expect(wrapper).toMatchSnapshot();
        expect(window.scrollTo).toHaveBeenCalledTimes(1);
        expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
    })
});