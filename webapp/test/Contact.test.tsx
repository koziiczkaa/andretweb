import {shallow, ShallowWrapper} from 'enzyme';
import {Contact} from '../src/components/Contact';
import React from 'react';


describe('Contact', () => {
    it("renders", ()=>{
        //when
        const wrapper: ShallowWrapper = shallow(<Contact/>);

        //then
        expect(wrapper).toMatchSnapshot();
    })

    it("name changes", ()=>{
        //given
        const wrapper: ShallowWrapper = shallow(<Contact/>);

        //when
        wrapper.find("#name").simulate("change", {
            target:{
                value: "Jan"
            }
        })

        //then
        expect(wrapper).toMatchSnapshot();
    })

    it("email changes", ()=>{
        //given
        const wrapper: ShallowWrapper = shallow(<Contact/>);

        //when
        wrapper.find("#email").simulate("change", {
            target:{
                value: "jankowalski@gmail.com"
            }
        })

        //then
        expect(wrapper).toMatchSnapshot();
    })

    it("message changes", ()=>{
        //given
        const wrapper: ShallowWrapper = shallow(<Contact/>);

        //when
        wrapper.find("#message").simulate("change", {
            target:{
                value: "Message test 123"
            }
        })

        //then
        expect(wrapper).toMatchSnapshot();
    })

    it("button click", ()=>{
        //given
        console.log = jest.fn();
        const wrapper: ShallowWrapper = shallow(<Contact/>);

        //when
        wrapper.find("#name").simulate("change", {
            target:{
                value: "Jan"
            }
        })

        wrapper.find("#email").simulate("change", {
            target:{
                value: "jankowalski@gmail.com"
            }
        })

        wrapper.find("#message").simulate("change", {
            target:{
                value: "Message test 123"
            }
        })

        wrapper.find("#button").simulate("click");

        //then
        expect(wrapper).toMatchSnapshot();
        expect(console.log).toHaveBeenCalledTimes(1);
        expect(console.log).toHaveBeenCalledWith("Jan", "jankowalski@gmail.com", "Message test 123");
    })
});
