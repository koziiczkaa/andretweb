import * as React from 'react';
import {shallow, ShallowWrapper} from 'enzyme';
import {Footer} from '../src/components/Footer';

describe('Footer', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<Footer/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
