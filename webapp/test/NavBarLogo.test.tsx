import {shallow, ShallowWrapper} from 'enzyme';
import {NavBarLogo} from '../src/components/NavBarLogo';
import React from 'react';

describe('NavBarLogo', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<NavBarLogo/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
