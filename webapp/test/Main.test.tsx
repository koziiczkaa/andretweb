import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {Main} from '../src/components/Main';

describe('Main', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<Main/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
