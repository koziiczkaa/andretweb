import {shallow, ShallowWrapper} from 'enzyme';
import {App} from '../src/components/App';
import React from 'react';

describe('App', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<App/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('changes theme to dark', () => {
        //given
        const wrapper: ShallowWrapper = shallow(<App/>);

        //when
        wrapper.find('#navbar').simulate('change', true);

        //then
        expect(wrapper).toMatchSnapshot();
    });

    it('changes theme to light', () => {
        //given
        const wrapper: ShallowWrapper = shallow(<App/>);

        //when
        wrapper.find('#navbar').simulate('change', false);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
