import {shallow, ShallowWrapper} from 'enzyme';
import {Recommended} from '../src/components/Recommended';
import React from 'react';

describe('Recommended', () => {
    it('renders', () => {
        //when
        const wrapper: ShallowWrapper = shallow(<Recommended/>);

        //then
        expect(wrapper).toMatchSnapshot();
    });
});
