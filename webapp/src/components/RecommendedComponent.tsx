import React from 'react';
import './RecommendedComponent.scss';
import {RecommendedItem} from '../entities/RecommendedItem';

export interface RecommendedItemProps {
    className?: string;
    recommendedItem: RecommendedItem;
}

export function RecommendedComponent(props: RecommendedItemProps) {
    return <a href={props.recommendedItem.link} target='_blank' className='recommended-link'>
        <div className='recommended-item'>
            <div className='recommended-image-section'>
                <img className='recommended-image' src={props.recommendedItem.img} alt=''/>
            </div>
            <div className='recommended-data'>
                <h4 className='recommended-name'>{props.recommendedItem.name}</h4>
                <span className='recommended-profession'>{props.recommendedItem.proffesion}</span>
            </div>
        </div>
    </a>;
}
