import React from 'react';
import './Quote.scss';

export interface QuoteProps {
    className?: string;
}

export function Quote(props: QuoteProps) {
    return <section className={props.className}>
        <p>"Każdy w tym kraju powinien wiedzieć jak zaprogramować komputer... Ponieważ to uczy jak myśleć"</p>
        <span>~ Steve Jobs</span>
    </section>;
}