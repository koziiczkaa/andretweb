import React from 'react';
import './ProjectsItem.scss';
import {Project} from '../entities/Project';
import {Link} from 'react-router-dom';

export interface ProjectsItemProps {
    project: Project;
}

export function ProjectsItem(props: ProjectsItemProps) {
    return <div className='project-item'>
        <div className='project-logo-div'>
            <img src={props.project.img} alt='' className='project-logo-img'/>
        </div>
        <div className='project-name-div'>
            <h3 className='project-title'>{props.project.title}</h3>
            <p className='project-desc'>{props.project.desc}</p>
            <Link to={'/projects/' + props.project.id}>
                <button className='project-button-readmore'>WIĘCEJ</button>
            </Link>

        </div>
    </div>;
}
