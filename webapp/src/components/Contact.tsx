import React from 'react';
import './Contact.scss';

export interface ContactProps {
    className?: string;
}

export function Contact(props: ContactProps) {

    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [message, setMessage] = React.useState('');

    function handleNameChange(event: React.ChangeEvent<HTMLInputElement>) {
        setName(event.target.value);
    }

    function handleEmailChange(event: React.ChangeEvent<HTMLInputElement>) {
        setEmail(event.target.value);
    }

    function handleMessageChange(event: React.ChangeEvent<HTMLTextAreaElement>) {
        setMessage(event.target.value);
    }

    function handleClick() {
        console.log(name, email, message);
    }

    return <section className={props.className}>
        <div className='contact-heading'>
            <span className='contact-title'>Napisz do mnie!</span>
        </div>
        <div className='form'>
            <form>
                <input type="text" id='name' className='form-input' name="name" placeholder="Jan Kowalski"
                       onChange={handleNameChange}/>
                <input type="email" id='email' className='form-input' name="email" placeholder="jankowalski@example.pl"
                       onChange={handleEmailChange}/>
                <textarea rows={8} id='message' className='form-message' name="message" placeholder="Wiadomość..."
                          onChange={handleMessageChange}/>
                <input type="button" id='button' value='WYŚLIJ' className='form-submit' onClick={handleClick}/>
            </form>
        </div>
    </section>;
}
