import React from 'react';
import './Main.scss';

export interface MainProps {
    className?: string;
}

export function Main(props: React.PropsWithChildren<MainProps>) {
    return <main className={props.className}>
        {props.children}
    </main>;
}