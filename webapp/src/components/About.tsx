import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import './About.scss';

export interface AboutProps {
    className?: string;
}

export function About(props: AboutProps) {
    return <section className={props.className}>
        <FontAwesomeIcon icon={faInfo} size='2x' color='#2CBFA4' className='info-icon'/>
        <span className='about-tittle'>O mnie</span>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum gravida sollicitudin lacus. Quisque
            porta maximus velit sed semper. Aliquam justo dolor, rutrum at commodo sed, mattis sit amet ligula. Nunc
            nec ligula iaculis, facilisis nisi eu, tempus augue. Mauris hendrerit ultricies risus sit amet mollis.
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce ut
            ex congue velit imperdiet mollis.</p>
    </section>;
}
