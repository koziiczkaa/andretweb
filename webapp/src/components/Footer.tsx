import React from 'react';
import './Footer.scss';

export function Footer() {
    return <footer className='footer'>
        <p className='footer-text'>Alicja Kozik &copy; 2020</p>
    </footer>;
}
