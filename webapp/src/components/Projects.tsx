import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faLaptopCode} from '@fortawesome/free-solid-svg-icons';
import './Projects.scss';
import {Link} from 'react-router-dom';

export interface ProjectsProps {
    className?: string;
}

export function Projects(props: ProjectsProps) {
    return <section className={props.className}>
        <Link to='/projects' className='projects-link'>
            <p className='projects-title'>Moje projekty</p>
            <FontAwesomeIcon icon={faLaptopCode} color='#2CBFA4' size='2x'/>
        </Link>
    </section>;
}