import React from 'react';

export interface ProjectReference {
    link: string;
    name: string;
    icon: React.ReactNode;
}