import React from 'react';
import {Project} from '../entities/Project';
import mockupProker from '../../resources/images/ProkerSite.png';
import './SingleProjectPage.scss';
import andretweb from '../../resources/images/AndretWeb.png';
import {ProjectReference} from "../entities/ProjectReference";
import {useParams} from 'react-router-dom';

export type Params = {
    id: string;
}

export interface ProjectPageProps {
    projects: Project[];
}

export function SingleProjectPage(props: ProjectPageProps) {
    const params: Params = useParams<Params>();

    const project: Project | undefined = props.projects.find(p => p.id === +params.id);

    const [image, setImage] = React.useState<string | undefined>(undefined);

    function handleClick(event: React.MouseEvent<HTMLImageElement, MouseEvent>): void {
        setImage(event.currentTarget.src);
    }

    function handleClose(): void {
        setImage(undefined);
    }

    React.useEffect(() => {
        window.scrollTo(0, 0);
    });

    return <>
        {image &&
        <>
            <div className='clicked-image'>
                <img src={andretweb} alt=''/>
            </div>
            <div className='fade' id='fscreenclose' onClick={handleClose}/>
        </>
        }
        <hr className='hr-classifier'/>
        <section className='project-page-wrapper'>
            <div className='project-col-1'>
                <img className='project-logo' src={project?.img} alt=''/>
            </div>
            <div className='project-col-2'>
                <h1 className='project-title'>{project?.title}</h1>
                <p>{project?.desc}</p>
                <div className='social-button-list'>
                    {project?.projectReference.map((projectReference: ProjectReference, index: number): JSX.Element => {
                        return <a href={projectReference.link} className='social-button' key={index}>
                            {projectReference.icon}
                            <span className='social-button-name'>{projectReference.name}</span>
                        </a>;
                    })}
                </div>
            </div>
        </section>
        <div className='project-col-3'>
            <img src={mockupProker} alt='' className='mockup-laptop' id='fscreenimg' onClick={handleClick}/>
        </div>
        <hr className='hr-classifier'/>
    </>;
}