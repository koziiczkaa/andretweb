import React from 'react';
import './ProjectsPage.scss';
import {Project} from '../entities/Project';
import {ProjectsItem} from '../components/ProjectsItem';

export interface ProjectsPageProps {
    project: Project[];
}

export function ProjectsPage(props: ProjectsPageProps) {
    React.useEffect(() => window.scrollTo(0, 0));

    return <>
        <hr className='hr-classifier'/>
        <section className='projects-wrapper'>
            <h1 className='projectsite-title'>Moje projekty</h1>
            {props.project.map((p: Project, index: number): JSX.Element => <ProjectsItem project={p}
                                                                                         key={index}/>)}
        </section>
        <hr className='hr-classifier'/>
    </>;
}